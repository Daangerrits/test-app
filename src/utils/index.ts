export const cleanString = (...args: any[]) =>
    args.filter(Boolean).join(" ")