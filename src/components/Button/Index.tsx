import React from "react"
import styles from "./styles.scss"

export const Button = ({ children, onclick }: any) =>
    <button className={styles.Button} type="button" onClick={onclick}>
        {children}
    </button>