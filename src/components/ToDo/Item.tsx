import React from "react"
import styles from "./styles.scss"
import { cleanString } from "./../../utils"
import { Button } from "../Button/Index"

export type ToDo = Pick<IToDoItem, "id" | "title" | "done">

export interface IToDoItem {
    id: string
    title: string
    done: boolean
    style: any
    ondelete: (id: string) => void, 
    ondone: (id: string) => void
}

export const ToDoItem = ({ id, title, done, ondelete, ondone, style }: IToDoItem) => {
    const toggle = () => ondone(id)
    const remove = () => ondelete(id)

    return <li id={id} className={cleanString(styles.ToDoItem, done && "-done")} style={style}>
        <span className="title">{title}</span>
        <span className="actions">
            <Button onclick={remove}>X</Button>
            <input className={styles.Checkbox} type="checkbox" checked={done} onChange={toggle} />
        </span>
    </li>
}