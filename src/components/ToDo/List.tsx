import React from "react"
import { cleanString } from "./../../utils"
import { ToDoItem, ToDo } from "./Item"
import styles from "./styles.scss"

interface ITodoList {
    todos: ToDo[]
    ondelete: (id: string) => void
    ondone: (id: string) => void
}

export const ToDoList = ({ todos, ondelete, ondone }: ITodoList) =>
    <ul className={cleanString(styles.ToDoList)}>
        {todos.map((todo, i) => 
            <ToDoItem key={todo.id} 
                style={{ backgroundColor: `rgb(255, ${105 + i * (150 / todos.length)}, 0)`}}
                ondelete={ondelete} 
                ondone={ondone} {...todo}
            />)}
    </ul>