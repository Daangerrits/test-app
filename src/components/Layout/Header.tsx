import React from "react"

export interface IHeader {
    total: number
}

export const Header = ({ total }: IHeader) =>
    <header className="Header">
        <h1>You have {total} todo's</h1>
    </header>