import React from "react"
import ReactDOM from "react-dom"
import { ToDoList } from "./components/ToDo/List"
import { Header } from "./components/Layout/Header"
import { v4 as uuidv4 } from "uuid"
import "./scss/base/_base.scss"

const initialState = [
    { id: uuidv4(), title: "Setup", done: true },
    { id: uuidv4(), title: "Commit", done: true },
    { id: uuidv4(), title: "Build UI", done: false },
    { id: uuidv4(), title: "Test", done: false },
    { id: uuidv4(), title: "Commit final", done: false },
]

const App = () => {
    const [todos, setTodos] = React.useState(initialState)
    const tasks = todos.filter(t => !t.done)
    const finished = todos.filter(t => t.done)

    const deleteToDo = (id: string) => 
        setTodos(todos.filter(todo => todo.id !== id))
    
    const finishToDo = (id: string) => 
        setTodos(todos.map(todo => todo.id === id 
            ? { ...todo, done: !todo.done } : todo))

    return <div className="App">
        <div className="container">
            <Header total={tasks.length} />                   
            <ToDoList todos={tasks} ondelete={deleteToDo} ondone={finishToDo} />
            <ToDoList todos={finished} ondelete={deleteToDo} ondone={finishToDo} />
        </div>
    </div>
}

ReactDOM.render(
    <App />, 
    document.getElementById("root")
)